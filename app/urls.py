
from lib.helpers.routing import get_class_view, get_view

urlpatterns = [
    get_view("signup"),
    get_view("create_website"),
    get_class_view("create_advertisement"),
    get_view("frame", url="frame/<int:website_id>"),
    get_view("click"),
    get_view("advertisement", url="advertisement/<int:ad_id>"),
    get_view("website", url="website/<int:website_id>"),
    get_view("pause_advertisement", url="toggle/<int:ad_id>"),
    get_view("stripe_success"),
    get_view("stripe_connect"),
    get_view("stripe_elements"),
    get_view("stripe_setup_advertiser"),
    get_view("clicks_api")
]
