from django.contrib import admin

from .models import AdClick, Advertisement, AdViewer, Profile, Website

# Register your models here.
models = [Advertisement, Website, AdViewer, AdClick, Profile]
admin.site.register(models)
