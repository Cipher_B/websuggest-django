
import os

from dateutil.relativedelta import relativedelta
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from geolite2 import geolite2

from app.models import AdClick, Advertisement, Website


class Command(BaseCommand):
    def _reset_database(self):
        try:
            filename = os.path.join(os.getcwd(), "db.sqlite3")
            os.remove(filename)
            self.stdout.write(
                self.style.SUCCESS("(SEED) ==> Database deleted.")
            )
        except FileNotFoundError:
            self.stdout.write(
                self.style.SUCCESS("(SEED) ==> Could not find database, moving on...")
            )

    def _create_superuser(self):
        User = get_user_model()
        User.objects.create_superuser("admin", "admin@websuggest.net", "password")
        self.stdout.write(
            self.style.SUCCESS(
                "(SEED) ==> Administrator created with username 'admin' and password 'password'."
            )
        )

    def _migrate(self):
        self.stdout.write(
            self.style.SUCCESS("(SEED) ==> Migrating database...")
        )
        call_command("migrate")

    def _sample_data(self):
        self.stdout.write(
            self.style.SUCCESS("(SEED) ==> Generating users...")
        )
        User = get_user_model()
        stalin = User.objects.create_user(
            "stalin",
            "stalin@websuggest.net",
            "password"
        )
        stalin.save()
        stalin.profile.stripe_customer = "tok_visa"
        stalin.profile.stripe_user_id = "acct_1Dd9YgBJToEunp1u"
        stalin.save()
        gorbachev = User.objects.create_user(
            "gorbachev",
            "gorbachev@websuggest.net",
            "password"
        )
        gorbachev.save()
        gorbachev.profile.stripe_customer = "tok_visa"
        gorbachev.profile.stripe_user_id = "acct_1Dd9YgBJToEunp1u"
        gorbachev.save()
        self.stdout.write(
            self.style.SUCCESS("(SEED) ==> Generating websites...")
        )
        google = Website.objects.create(
            url="https://google.com",
            user_id=2,
            views=88
        )
        google.save()
        duckduckgo = Website.objects.create(
            url="https://duckduckgo.com",
            user_id=3,
            views=88
        )
        duckduckgo.save()
        self.stdout.write(
            self.style.SUCCESS("(SEED) ==> Generating advertisements...")
        )
        reforms = Advertisement.objects.create(
            title="Gorbachev's reforms",
            body="His 3 reforms will shock you!",
            user_id=3,
            passed=True,
            target_url="https://en.wikipedia.org/wiki/Perestroika",
            views=88
        )
        reforms.save()
        socialism = Advertisement.objects.create(
            title="Socialism will win!",
            body="Two successful five-year plans have improved Soviet production by 200%!",
            user_id=2,
            passed=True,
            target_url="https://en.wikipedia.org/wiki/First_five-year_plan",
            views=88
        )
        socialism.save()
        self.stdout.write(
            self.style.SUCCESS("(SEED) ==> Generating clicks...")
        )
        reader = geolite2.reader()
        click0 = AdClick.objects.create(
            advertisement_id=2,
            website_id=2,
            location=reader.get("142.17.170.187"),
            datetime=now(),
            price=1.0
        )
        click2 = AdClick.objects.create(
            advertisement_id=1,
            website_id=2,
            location=reader.get("131.217.34.129"),
            datetime=now() + relativedelta(months=-4),
            price=1.0
        )
        click2 = AdClick.objects.create(
            advertisement_id=1,
            website_id=2,
            location=reader.get("131.217.34.129"),
            datetime=now() + relativedelta(months=-4, days=5),
            price=1.0
        )
        click3 = AdClick.objects.create(
            advertisement_id=1,
            website_id=1,
            location=reader.get("149.140.155.165"),
            datetime=now(),
            price=1.3
        )
        click0.save()
        click2.save()
        click3.save()

    def _seed(self):
        self._reset_database()
        self._migrate()
        self._create_superuser()
        self._sample_data()

    def handle(self, *args, **options):
        self._seed()
