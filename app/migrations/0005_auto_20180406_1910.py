# Generated by Django 2.0.4 on 2018-04-06 19:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_advertisement_passed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advertisement',
            name='body',
            field=models.TextField(max_length=180),
        ),
        migrations.AlterField(
            model_name='advertisement',
            name='title',
            field=models.CharField(max_length=25),
        ),
    ]
