from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views import generic

from ..forms import CreateAdvertisementForm


class CreateAdvertisementView(generic.FormView):
    template_name = 'create_advertisement.html'
    form_class = CreateAdvertisementForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        model_instance = form.save(commit=False)
        model_instance.user = self.request.user.profile
        model_instance.save()
        url = reverse("advertisement", kwargs={
            "ad_id": str(model_instance.id)
        })
        return redirect(url)
