from django.shortcuts import redirect, render

from lib.logic.total_balance import total_balance


def HomeView(request):
    if request.user.is_authenticated:
        if request.user.profile.stripe_user_id == "0" and request.user.profile.stripe_customer == "0":
            return render(request, "onboard.html", {
            })
        else:
            website = request.user.profile.website_set.first()
            if website is not None:
                return redirect("website", website.id)
            advertisement = request.user.profile.advertisement_set.first()
            if advertisement is not None:
                return redirect("advertisement", advertisement.id)
            else:
                return render(request, "dashboard.html", {
                    "total_balance": total_balance(request.user.profile)
                })
    else:
        return redirect("promotion")
