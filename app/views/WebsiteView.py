from django.shortcuts import redirect, render

from lib.logic.conversion_rate import calc_conversion_rate
from lib.logic.total_balance import total_balance

from ..models import Website


def WebsiteView(request, website_id):
    if request.user.is_authenticated:
        website_object = Website.objects.get(id=website_id)
        if website_object.user == request.user.profile:
            total_clicks = 0
            for click in website_object.adclick_set.all():
                total_clicks += 1
            return render(request, "website_view.html", {
                "current_website": website_object,
                "total_clicks": total_clicks,
                "conversion_rate": calc_conversion_rate(total_clicks, website_object.views),
                "total_balance": total_balance(request.user.profile)
            })
        else:
            return redirect("home")
    else:
        return redirect("home")
