import json

import requests
from django.contrib import messages
from django.shortcuts import redirect

from websuggest import settings


def StripeSuccessView(request):
    error = request.GET.get("error")
    if error:
        error_description = request.GET.get("error_description")
        messages.add_message(request, messages.ERROR, f"Failed to connect with Stripe: {error_description}")
        print("(stripe-connect) OAuth Error: " + error_description)
        return redirect("home")
    else:
        response = requests.post("https://connect.stripe.com/oauth/token", data={
            "client_secret": settings.STRIPE_SECRET_KEY,
            "code": request.GET.get("code"),
            "grant_type": "authorization_code"
        })
        if response.status_code == requests.codes.ok:
            parsed = json.loads(response.text)
            print("(stripe-connect) New user " + parsed["stripe_user_id"])
            user = request.user
            user.profile.stripe_user_id = parsed["stripe_user_id"]
            user.save()
            messages.add_message(request, messages.SUCCESS, "Your account has been connected with Stripe successfully")
            return redirect("create_website")
        else:
            messages.add_message(request, messages.ERROR, "Failed to connect with Stripe")
            print("(stripe-connect) API Error " + response.status_code)
            return redirect("home")
        return redirect("home")
