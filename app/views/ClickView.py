from geolite2 import geolite2
import pytz
from django.conf import settings
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.datetime_safe import datetime

from util import get_client_ip

from ..models import AdClick, Advertisement, AdViewer, Website


class ClickfraudProtect:
    def __init__(self, request, client_ip):
        self.request = request
        self.client_ip = client_ip

    def referer_is_incorrect(self, website):
        actual_referer = self.request.META.get('HTTP_REFERER')
        correct_referer = self.request.build_absolute_uri(reverse('frame', kwargs={
            "website_id": website
        }))
        if actual_referer == correct_referer:
            return False
        else:
            print("Access Denied: referer is incorrect")
            return True

    def user_is_spammer(self):
        try:
            adviewer = AdViewer.objects.get(ip=self.client_ip)
            # Found matching IP
            elapsed_since_last_click = datetime.now(pytz.utc) - adviewer.last_click
            if adviewer.ad_clicks >= 500:
                # Exceeded limit
                print("Access Denied: click limit is exceeded")
                return True
            elif elapsed_since_last_click.total_seconds() <= 30:
                # Exceeded timed click limit
                print("Access Denied: click frequency is too high")
                return True
            else:
                adviewer.ad_clicks += 1
                adviewer.last_click = datetime.now(pytz.utc)
                adviewer.save()
                return False
        except AdViewer.DoesNotExist:
            # Did not find matching IP
            adviewer_new = AdViewer()
            adviewer_new.ip = self.client_ip
            adviewer_new.ad_clicks = 1
            adviewer_new.last_click = datetime.now(pytz.utc)
            adviewer_new.save()
            return False


reader = geolite2.reader()


def ClickView(request):
    website = request.GET.get("w_id")
    ad = request.GET.get("ad_id")
    website_object = Website.objects.get(id=website)
    ad_object = Advertisement.objects.get(id=ad)
    client_ip = get_client_ip(request)
    clickfraud_protect = ClickfraudProtect(
        request,
        client_ip
    )
    # only trigger clickfraud-protect in production
    if not settings.DEBUG:
        if clickfraud_protect.referer_is_incorrect(website) or clickfraud_protect.user_is_spammer():
            return render(request, "forbidden.html", status=403)
    click_object = AdClick()
    click_object.advertisement = ad_object
    click_object.website = website_object
    click_object.location = reader.get(client_ip)
    click_object.location = reader.get(client_ip)
    click_object.datetime = datetime.now(pytz.utc)
    total_advertisers = Advertisement.objects.all().count()
    total_websites = Website.objects.all().count()
    price = 1 + 0.3 * (total_advertisers - total_websites)
    if price < 0.1:
        click_object.price = 0.1
    elif price > 40:
        click_object.price = 40
    else:
        click_object.price = price
    click_object.save()
    return redirect(ad_object.target_url)