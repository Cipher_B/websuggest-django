
from django.shortcuts import render

from websuggest import settings


def StripeElementsView(request):
    return render(request, "stripe_elements.html", {
        "stripe_public_key": settings.STRIPE_PUBLIC_KEY
    })
