
from pathlib import Path
from importlib import import_module

files = Path(__file__).parent.glob("*.py")
for file_path in files:
    if not file_path.name.startswith("_"):
        file_name = file_path.stem
        module = import_module(f".{file_name}", __package__)
        view = getattr(module, file_name)
        exec(f"{file_path.stem} = view")
