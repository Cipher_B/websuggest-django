
from django.shortcuts import render

from websuggest import settings


def StripeConnectView(request):
    url = "https://connect.stripe.com/oauth/authorize"
    params = "?response_type=code&client_id={}&scope=read_write".format(
        settings.STRIPE_CLIENT_ID
    )
    return render(request, "connect_with_stripe.html", {
        "redirect": url + params
    })
