from django.test import TestCase
from app.models import AdClick
from app.models import Transaction
from app.models import Advertisement
from app.models import Profile
from django.contrib.auth import get_user_model
from app.models import Website
from datetime import datetime as DateTime
from decimal import Decimal

class TransactionTest(TestCase):

    def test_transaction(self):
        User = get_user_model()
        u = User.objects.create_user("a","b","c")
        p = u.profile
        a = Advertisement()
        a.user = p
        a.save()
        w = Website(user=p)
        w.save()
        t = Transaction()
        t.save()
        ac1 = AdClick()
        ac1.price = 1.20
        ac1.advertisement = a
        ac1.website = w
        ac1.datetime = DateTime(2019,6,5)
        ac1.save()
        t.advertisement_charge_adclick_set.add(ac1)
        self.assertEqual(t.advertisement_charge_amount(), Decimal('1.2'))
