# WebSuggest
[![pipeline status](https://gitlab.com/websuggest/websuggest-django/badges/master/pipeline.svg)](https://gitlab.com/websuggest/websuggest-django/commits/master)

![logo](https://gitlab.com/websuggest/websuggest-django/raw/master/static/img/SuggestionsLogo.png?inline=false)

WebSuggest is an open-source advertising network that focuses on privacy, security and user-friendliness. It attempts to bring Ethical Design to the field of internet advertising.

Major features:
* All advertisements are hand-reviewed.
* No cookies, no fingerprinting, no tracking.
* Very strict clickfraud detection system.
* AGPL means Free as in Freedom

🚧 WebSuggest is under development and highly unstable. 🚧

# Get Involved
Have any questions or comments? Want to contribute to the project? Feel free to open an issue, email me at (jwinnie AT stormdesign DOT us), or message me on [Mastodon](https://mastodon.cloud/@jwinnie)!

# Development
WebSuggest uses Python/Django on the server side and Vue.js/Webpack on the client side. To get started, install [nvm](https://github.com/nvm-sh/nvm) ([fish-nvm](https://github.com/jorgebucaran/fish-nvm) if you use the fish shell, which you should because it's cool) and [pyenv](https://github.com/pyenv/pyenv). Then, run the following commands to install the correct versions of Node.js, Python, Pipenv, and Yarn:
```bash
$ nvm install
$ pyenv install
$ pip3 install pipenv
$ npm install -g yarn
```

Once you've done this, you can install dependencies using:
```bash
$ make setup
```

To run the test suite:

```bash
$ make test
```

To run the development server:
```bash
$ make dev
```

You'll also need a `.env` file in order to test Stripe. It should look like this:
```
STRIPE_PUBLIC_KEY=
STRIPE_SECRET_KEY=
STRIPE_CLIENT_ID=
```
Create an account on Stripe and fill in those fields with your keys.

## Seed data
`make setup` generates three users: `admin`, `stalin` and `gorbachev`. All users have a password of `password`.

If you'd like to reset the database and reload the seed data, run `pipenv run python manage.py seed`.

## Frontend
Most of the frontend stuff is in `/frontend/` and the HTML is in `/templates/` (they're Django templates). Images go in `/static/img`.

To create a production build:
```bash
$ make build
```
The frontend pipeline is based on Webpack; you can read `webpack.config.js` for the precise configuration.

Basically:
* All CSS is autoprefixed with Autoprefixer and all JavaScript is processed through Babel.
* SASS/SCSS is optional but recommended.
* Turbolinks is installed, so use `document.addEventListener("turbolinks:load")` to wait until page load, and beware of many quirks, etc.
* Vue is used only on the dashboard. All Vue template code is directly embedded in the HTML in `/templates/`.

## Backend
Views go in the `/app/views/` directory. All views must be in title case and must end with the word `View`. This is because the URL helper automatically converts URL slugs to view names that way. In the URLs file, there are two main helper functions: `get_view` (for function-based views) and `get_class_view` (for class-based views).

The settings file also uses custom code. To add a setting, simply do this:
```python
SETTING_NAME = s.env("ENVIRONMENT_VARIABLE_NAME", DEFAULT_VALUE)
```
Usually the default value is used for development, and the production server has an environment variable set to override that value. If the setting is required, simply leave out the default value.

## Runnning PostgreSQL locally

By default, the app uses SQLite3 in development, but if you would like to test out PostgreSQL (which is used in production), you need to first install PostgreSQL, and then run the following commands to create the user and the database:

```bash
$ psql -c "CREATE USER websuggest WITH CREATEDB PASSWORD 'password';"
$ createdb -U websuggest -W websuggest
```

> On Linux it is often necessary to prefix the above commands with `sudo -u postgres` because your login user likely has insufficient permissions to access the database.

Then run:

```bash
$ export DATABASE_URL=postgresql://localhost/websuggest?user=websuggest&password=password make dev
```