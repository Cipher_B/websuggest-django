
from django.contrib import admin
from django.contrib.auth import views
from django.urls import include, path

from lib.helpers.routing import get_template_view, get_view

authurls = [
    path('login/', views.LoginView.as_view(redirect_authenticated_user=True), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]

urlpatterns = [
    get_view("home", url=""),
    path("", include("app.urls")),
    path("site-admin/", admin.site.urls),
    path("accounts/", include(authurls)),
    get_template_view("beta_warning"),
    get_template_view("promotion"),
    get_template_view("publisher_docs")
]
