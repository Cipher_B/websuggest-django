def total_balance(profile):
    total_balance = 0
    for website in profile.website_set.all():
        for click in website.adclick_set.all():
            total_balance += click.price
    for advertisement in profile.advertisement_set.all():
        for click in advertisement.adclick_set.all():
            total_balance -= click.price
    return total_balance
