
def calc_conversion_rate(clicks, views):
    if clicks != 0:
        conversion_rate_raw = (clicks / views) * 100
        return "{0:g}".format(conversion_rate_raw)
    else:
        return 0
