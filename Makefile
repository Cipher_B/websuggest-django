setup:
	pipenv install
	yarn
	pipenv run python manage.py seed

dev: 
	npx concurrently --save-dev "pipenv run python manage.py runserver" "yarn dev"

test:
	pipenv run python manage.py test

build:
	yarn build
