'''
Uses the Stripe API to pay people and receive payment. Meant to be run daily,
but checks to see that it's the end of the month.

Uses:
 - User.objects.all to call users table
 - 30 percent commission is hard-coded

'''



import os

import django
import requests


os.environ["DJANGO_SETTINGS_MODULE"] = "websuggest.settings"

django.setup()

from app.models import Website, Advertisement  # NOQA
from django.contrib.auth.models import User  # NOQA
from django.conf import settings  # NOQA
from datetime import datetime  # NOQA
from sys import exit  # NOQA
from decimal import Decimal  # NOQA

for user in User.objects.all():
    total_balance = Decimal(0)
    for website in user.profile.website_set.all():
        for click in website.adclick_set.all():
            total_balance += click.price * Decimal(0.7)
    for advertisement in user.profile.advertisement_set.all():
        for click in advertisement.adclick_set.all():
            total_balance -= click.price
    print("(charge-bot) Total balance of {} is {}".format(user, total_balance))
    if total_balance < 0:
        if datetime.today().day != 1 and not settings.DEBUG:
            print("Wrong day; exiting")
            exit()
        print(f"(charge-bot) User {str(user)}: Recieving money")
        response = requests.post("https://api.stripe.com/v1/charges", data={
            "amount": abs(int(total_balance * 100)),
            "currency": "usd",
            "source": user.profile.stripe_customer
        }, auth=(settings.STRIPE_SECRET_KEY, ""))
        print(response.text)
    if total_balance > 0:
        if datetime.today().day != 20 and not settings.DEBUG:
            print("Wrong day; exiting")
            exit()
        print(f"(charge-bot) User {str(user)}: Sending money")
        response = requests.post(
            "https://api.stripe.com/v1/transfers",
            data={
                "amount": int(total_balance * 100),
                "currency": "usd",
                "destination": user.profile.stripe_user_id
            },
            auth=(settings.STRIPE_SECRET_KEY, "")
        )
        print(response.text)
