/**
import tippy from 'tippy.js/dist/tippy';
import 'tippy.js/dist/tippy.css';
import { DataTable } from "simple-datatables";

function geoip(axios) {
    for (let i of document.getElementsByClassName("geoip")) {
        let url = `https://ipinfo.io/${i.innerHTML}?token=7058f9921d2f5b`;
        console.log(url);
        axios.get(url)
            .then(response => {
                console.log("geoip: success")
                if (response.data.city && response.data.region && response.data.country) {
                    i.innerHTML += `<br/><i class="fas fa-map-marked" id="tooltip_geoip"></i>&nbsp;${response.data.city}, ${response.data.region} (${response.data.country})`;
                    tippy("#tooltip_geoip", {
                        performance: true,
                        content: "Geolocation provided by the ipinfo service. Not guaranteed to be accurate.",
                        animation: "scale",
                        duration: 50
                    });
                }
            })
            .catch(error => {
                console.log("geoip: failure");
                i.innerHTML = `${i.innerHTML}<br/><span class="error">Couldn't fetch location information.</span>`;
                tippy(".error", {
                    performance: true,
                    content: `We encountered an error: "${error}". Is your ad blocker or content filter blocking ipinfo.io?`,
                    animation: "shift-away",
                    duration: 50,
                    arrow: true
                })
            });
    }
}
**/

// function datatable__setup() {
    // import(/* webpackChunkName: "axios" */ "axios").then(axios => geoip(axios));
    // new DataTable("#click_table");
    // tippy("#tooltip", {
        // animation: "scale",
        // interactive: true,
        // duration: 100,
        // performance: true,
        // content: "<span class='font-size:30px !important'>Click prices are calculated in real time through an analysis of network supply and demand. They are uniform throughout the network, but may change often. <b>If you believe you have been charged unfairly, please contact us.</b></span>"
    // });
    // tippy("#pause", {
        // animation: "scale",
        // arrow: true,
        // duration: 200,
        // performance: true,
        // content: "This will stop the ad from being shown on the network. You may unpause at any time."
//     })
//     let input = document.querySelector(".dataTable-input").classList;
//     input.add("form-control");
//     input.add("datatable-search");
// }

// document.addEventListener("turbolinks:load", datatable__setup);

import tippy from 'tippy.js/dist/tippy';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/dist/themes/light-border.css';

import Vue from 'vue/dist/vue.js';
import TurbolinksAdapter from 'vue-turbolinks';
Vue.use(TurbolinksAdapter);

import { LMap, LTileLayer, LMarker, LPopup } from 'vue2-leaflet';
import { Icon } from 'leaflet';
import 'leaflet/dist/leaflet.css';

import ClipboardJS from "clipboard";

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-popup', LPopup);

delete Icon.Default.prototype._getIconUrl;

import VueChartkick from 'vue-chartkick';
import Chart from 'chart.js';

Vue.use(VueChartkick, { adapter: Chart })

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

document.addEventListener("turbolinks:load", () => {
    tippy("#pending-review", {
        animation: "shift-away",
        animateFill: false,
        theme: "light-border",
        interactive: true,
        duration: 100,
        content: "<div style='display: flex; align-items: center'><img src='/static/img/chip-head.png' style='height: 80px'></img> <p style='text-align: left'>This advertisement is currently undergoing human review and will only be shown after it has passed. If you have questions about the status of manual review, please reach out to us.</p></div>"
    })
    tippy("#pause_ad", {
        animation: "shift-away",
        animateFill: false,
        theme: "light-border",
        duration: 100,
        content: "⏸️<br/>Click to temporarily withdraw the advertisement from the network."
    })
    new Vue({
        delimiters: ["<%", "%>"],
        el: "#vue-app",
        data: {
            api: {},
            bounds: L.latLngBounds([
                [-89.98155760646617, -180], [89.99346179538875, 180]
            ])
        },
        computed: {
            click_rate() {
                if (this.api.total_views == 0) {
                    return (0).toLocaleString("en", {style: "percent"})
                } else {
                    return (this.api.total_clicks / this.api.total_views).toLocaleString("en", {style: "percent"})
                }
            },
            clicks_geolocated() {
                if (this.api.clicks !== undefined) return this.api.clicks.filter((click) => click.location !== null)
            }
        },
        methods: {
            query_api() {
                let id = window.location.pathname.split("/").slice(-1)[0];
                let ad_or_website = window.location.pathname.split("/").slice(-2)[0];
                let url;
                switch(ad_or_website) {
                    case "advertisement":
                        url = "/clicks-api?for_ad=" + id;
                        break;
                    case "website":
                        url = "/clicks-api?for_website=" + id;
                        break;
                }
                fetch(url).then((res) => {
                    if (res.ok) {
                        res.json().then((parsed) => {
                            this.api = parsed;
                        })
                    } else {
                        console.log(res.status)
                    }
                }).catch((err) => console.log(err));
            },
            get_price(click) {
                return click.price.toLocaleString("en-US", {
                    style: "currency",
                    currency: "USD"
                })
            }
        },
        mounted() {
            this.query_api();
            setInterval(() => {
                this.query_api();
            }, 3000)
        }
    });
    tippy("#clipboard-trigger", {
        trigger: "click",
        animation: "shift-away",
        animateFill: false,
        duration: 200,
        content: "Copied!"
    });
    new ClipboardJS("#clipboard-trigger");
})
