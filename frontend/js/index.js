
import "~css/main.sass"

import * as Turbolinks from "turbolinks";
import hoverintent from "hoverintent";

Turbolinks.start();
Turbolinks.setProgressBarDelay(50);

// Turbolinks prefetch (see https://www.mskog.com/posts/instant-page-loads-with-turbolinks-and-prefetch/)
document.addEventListener("turbolinks:load", () => {
    var hoverIntentOptions = {
      interval: 50,
      sensitivity: 5
    };
    document.querySelectorAll("a").forEach(node => {
        if (node.dataset.turbolinks === "false") {
            return;
        }
        var prefetcher;
        hoverintent(
            node,
            function() {
                var href = this.getAttribute("href");
                if (!href.match(/^\//)) {
                    return;
                }
                if (prefetcher) {
                    if (prefetcher.getAttribute("href") != href) {
                        prefetcher.getAttribute("href", href);
                    }
                } else {
                    var link = document.createElement("link");
                    link.setAttribute("rel", "prefetch");
                    link.setAttribute("href", href);
        
                    prefetcher = document.body.appendChild(link);
                }
            },
            function() {}
        ).options(hoverIntentOptions);
    });
});